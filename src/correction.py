import random as rd

class Simulation() :
    _NB_THONS = 8
    _NB_REQUINS = 1
    def __init__(self,n) :
        """
        :param n: (int) nombre de tours à simuler
        :return: None
        :CU: n > 0
        """
        self._nb_tours = n
        self._mer =  Mer()
        self.init_thons()
        self.init_requins()
        
    def init_thons (self):
        """
        instancie _NB_THONS occurences de Thon dans la mer à des emplacement vide
        """
        for i in range (0, self._NB_THONS):
            x,y = self._mer.coordonnees_aleatoire_eau()
            self._mer._elements[x][y] = Thon()
            
    def init_requins (self):
        """
        instancie _NB_REQUINS occurences de Requin dans la mer à des emplacement vide
        """
        for i in range (0, self._NB_REQUINS):
            x,y = self._mer.coordonnees_aleatoire_eau()
            self._mer._elements[x][y] = Requin()
    
    def simuler_un_tour(self) :
        """
        Choisi un élément aléatoire dans la mer,
            Si l'élément choisi est de l'eau, il ne se passe rien,
            Si l'élément choisi est un thon,
                il se déplace si cela est possible, c'est a dire si un de ces voisins est de l'eau
                    On fait alors appel sa méthode se_deplacer en donnant comme paramètres la liste de ses eaux voisines et la liste vide qui représente les thons voisins, le tuple d'entier renvoyé par cette méthode sont les nouvelles coordonnées du thon dans la mer
                il reste sur place sinon
                si il donne naissance, un nouveau thon est instancier à la position précedante du thon parent
                sinon le thon est remplacé par de l'eau
            si l'élément choisi est un requin,
               il se déplace si cela est possible, c'est a dire si un de ces voisins est de l'eau ou un thon en privilégiantles déplacements vers les voisins qui sont des thons qui leur apporte +1 d'énergie
                    c'est a dire que l'on fait appel sa méthode se_deplacer en donnant comme paramètres la liste de ses eaux voisines et la liste de ces thons voisins, le tuple d'entier renvoyé par cette méthode sont les nouvelles coordonnées du requin dans la mer
                il reste sur place sinon
                si il donne naissance, un nouveau requin est instancier à la position précédante du requin parent
                sinon le requin est remplacé par de l'eau
                si le requin est mort, alors de l'eau vient remplacé le requin dans la mer
        """
        x,y = self._mer.coordonnees_aleatoire()
        element = self._mer._elements[x][y]
        if element != None:
            if element.est_thon() :
                eaux_voisines = self._mer.coordonnees_eaux_voisines(x,y)
                if eaux_voisines :
                    dx,dy = element.se_deplacer(eaux_voisines,[]) 
                    self._mer._elements[dx][dy] = element
                    if element.donne_naissance():
                        self._mer._elements[x][y] = Thon()
                    else:
                        self._mer._elements[x][y] = None
            elif element.est_requin() :
                eaux_voisines = self._mer.coordonnees_eaux_voisines(x,y)
                thons_voisins = self._mer.coordonnees_thons_voisins(x,y)
                if eaux_voisines or thons_voisins:
                    dx,dy = element.se_deplacer(eaux_voisines,thons_voisins)
                    self._mer._elements[dx][dy] = element
                    if element.donne_naissance():
                        self._mer._elements[x][y] = Requin()
                    else:
                        self._mer._elements[x][y] = None
                    if element.est_mort() :
                        self._mer._elements[dx][dy] = None
                
            
    def lancer_simulation(self) :
        """
        Affiche la mer puis effectue _nb_tours simulations une à une.
        Après chaque tour simulé, cette methode affiche le numéro de tour ainsi que la mer.
        """
        print(self._mer)
        for i in range (0,self._nb_tours):
            self.simuler_un_tour()
            print('Tour '+str(i+1)+' : ')
            print(self._mer)
    
    
    
class Mer() :
    _TAILLE = 5
    def __init__(self) :
        self._elements = [ [None for _ in range (self._TAILLE) ] for _ in range (self._TAILLE)]
    
    def coordonnees_aleatoire(self) :
        """
        :return: (tuple) une paire x,y désignant les coordonnées d'un élément aléatoire dans la mer
        :Exemples:
        >>> mer = Mer()
        >>> x,y = mer.coordonnees_aleatoire()
        >>> x < 5
        True
        >>> x >= 0
        True
        >>> y < 5
        True
        >>> y >= 0
        True
        """
        return rd.randint(0,self._TAILLE-1), rd.randint(0,self._TAILLE-1)
    
    def coordonnees_aleatoire_eau(self):
        """
        :return: (tuple) une paire x,y désignant les coordonnées d'un élément aléatoire de la mer représentant de l'eau
        :Exemples:
        >>> mer = Mer()
        >>> x,y = mer.coordonnees_aleatoire()
        >>> x < 5
        True
        >>> x >= 0
        True
        >>> y < 5
        True
        >>> y >= 0
        True
        >>> mer._elements[x][y] == None
        True
        """
        x,y = self.coordonnees_aleatoire()
        while not self._elements[x][y] == None:
            x,y = self.coordonnees_aleatoire()
        return x,y
        
    
    def coordonnees_eaux_voisines(self,x,y) :
        """
        :param x: (int) un entier désignant une des coordonnées d'un élément
        :param y: (int) un entier désignant l'autre coordonnée de l'élément
        :return: (list) la liste des eaux voisines a l'élément dont les coordonnées sont x et y
        :CU: x >= 0 and x < self._TAILLE and y >= 0 and y < self._TAILLE
        """
        l = [((x-1) % self._TAILLE, y), ((x+1) % self._TAILLE, y), (x, (y-1) % self._TAILLE), (x, (y+1) % self._TAILLE)]
        for (a,b) in l:
            if not self._elements[a][b] == None :
                l.remove((a,b))
        return l
    
    def coordonnees_thons_voisins(self,x,y) :
        """
        :param x: (int) un entier désignant une des coordonnées d'un élément
        :param y: (int) un entier désignant l'autre coordonnée de l'élément
        :return: (list) la liste des thons voisins a l'élément dont les coordonnées sont x et y
        :CU: x >= 0 and x < self._TAILLE and y >= 0 and y < self._TAILLE
        """
        l = [((x-1) % self._TAILLE, y), ((x+1) % self._TAILLE, y), (x, (y-1) % self._TAILLE), (x, (y+1) % self._TAILLE)]
        for (a,b) in l:
            if self._elements[a][b] == None:
                l.remove((a,b))
            elif not self._elements[a][b].est_thon() :
                l.remove((a,b))
        return l
    
    def __str__(self) :
        """
        :return: (str) une chaine de caractère représentant la mer
        """
        s=''
        for i in range (0,self._TAILLE):
            for j in range(0,self._TAILLE):
                if self._elements[i][j] == None :
                    s+=' _'
                else : 
                    s+=' '+str(self._elements[i][j])
            s+='\n'
        return s
    
    
class Poisson() :
    def __init__(self) :
        pass
    
    def est_thon(self) :
        """
        :return: (bool) retourne vrai si le contenu est un thon, faux sinon
        :Exemples:
        >>> thon = Thon()
        >>> thon.est_thon()
        True
        >>> requin = Requin()
        >>> requin.est_thon()
        False
        """
        return isinstance(self, Thon)
    
    def est_requin(self) :
        """
        :return: (bool) retourne vrai si le contenu est un requin, faux sinon
        :Exemples:
        >>> thon = Thon()
        >>> thon.est_requin()
        False
        >>> requin = Requin()
        >>> requin.est_requin()
        True
        """
        return isinstance(self, Requin)
    
    def se_deplacer(self, eaux_voisines,thons_voisins) :
        """
        :param eaux_voisines: (list) la liste des coordonnées sous forme de tuple des eaux voisines au poisson
        :param thons_voisins: (list) la liste des coordonnées sous forme de tuple des thons voisins
        :return: (tuple) un couple x,y des coordonnées du poisson  après son déplacement
        """
        if self.est_requin():
            self._energie = self._energie-1
            if len(thons_voisins) != 0 :
                self._energie = self._energie + 2
                return rd.choice(thons_voisins)
        else :
            return rd.choice(eaux_voisines)
    
    def donne_naissance(self) :
        """
        :return: (bool) retourne vrai si le poisson a donner naissance a un nouveau poisson, faux sinon
        :Exemples:
        >>> requin = Requin()
        >>> requin.donne_naissance()
        False
        >>> requin.donne_naissance()
        False
        >>> requin.donne_naissance()
        False
        >>> requin.donne_naissance()
        False
        >>> requin.donne_naissance()
        True
        >>> requin.donne_naissance()
        False
        >>> thon = Thon()
        >>> thon.donne_naissance()
        False
        >>> thon.donne_naissance()
        True
        >>> thon.donne_naissance()
        False
        """
        self._temps_gestation  = self._temps_gestation - 1
        if self._temps_gestation == 0 :
            self._temps_gestation = self._DUREE_GESTATION
            return True
        else :
            return False
    
    def __str__(self) :
        pass
    
class Thon(Poisson) :
    _DUREE_GESTATION = 2
    def __init__(self) :
        self._temps_gestation = self._DUREE_GESTATION        
    
    def __str__(self) :
        """
        :return: (str) une chaine représentant le thon
        :Exemples:
        >>> thon = Thon()
        >>> str(thon)
        'T'
        """
        return 'T'
        
class Requin(Poisson) :
    _ENERGIE = 3
    _DUREE_GESTATION = 5
    def __init__(self) :
        self._temps_gestation = self._DUREE_GESTATION
        self._energie = self._ENERGIE
    
    def est_mort(self) :
        """
        :return: (bool) retourne vrai si le requin est mort, faux sinon
        :Exemples:
        >>> requin = Requin()
        >>> requin.est_mort()
        False        
        """
        return self._energie == 0
    
    def __str__(self) :
        """
        :return: (str) une chaîne représentant le requin
        :Exemples:
        >>> requin = Requin()
        >>> str(requin)
        'R'
        """
        return 'R'

import doctest
doctest.testmod(optionflags=doctest.NORMALIZE_WHITESPACE | doctest.ELLIPSIS, verbose=True)


