class Simulation() :
    def __init__(self,n) :
        """
        :param n: (int) nombre de tours à simuler
        :return: None
        :CU: n > 0
        """
        pass
        
    def init_thons (self):
        """
        instancie _NB_THONS occurences de Thon dans la mer à des emplacement vide
        """
        pass
            
    def init_requins (self):
        """
        instancie _NB_REQUINS occurences de Requin dans la mer à des emplacement vide
        """
        pass
    
    def simuler_un_tour(self) :
        """
        Choisi un élément aléatoire dans la mer,
            Si l'élément choisi est de l'eau, il ne se passe rien,
            Si l'élément choisi est un thon,
                il se déplace si cela est possible, c'est a dire si un de ces voisins est de l'eau
                    On fait alors appel sa méthode se_deplacer en donnant comme paramètres la liste de ses eaux voisines et la liste vide qui représente les thons voisins, le tuple d'entier renvoyé par cette méthode sont les nouvelles coordonnées du thon dans la mer
                il reste sur place sinon
                si il donne naissance, un nouveau thon est instancier à la position précedante du thon parent
                sinon le thon est remplacé par de l'eau
            si l'élément choisi est un requin,
               il se déplace si cela est possible, c'est a dire si un de ces voisins est de l'eau ou un thon en privilégiantles déplacements vers les voisins qui sont des thons qui leur apporte +1 d'énergie
                    c'est a dire que l'on fait appel sa méthode se_deplacer en donnant comme paramètres la liste de ses eaux voisines et la liste de ces thons voisins, le tuple d'entier renvoyé par cette méthode sont les nouvelles coordonnées du requin dans la mer
                il reste sur place sinon
                si il donne naissance, un nouveau requin est instancier à la position précédante du requin parent
                sinon le requin est remplacé par de l'eau
                si le requin est mort, alors de l'eau vient remplacé le requin dans la mer
        """
        pass 
            
    def lancer_simulation(self) :
        """
        Affiche la mer puis effectue _nb_tours simulations une à une.
        Après chaque tour simulé, cette methode affiche le numéro de tour ainsi que la mer.
        """
        pass

    
class Mer() :
    def __init__(self) :
        pass
    
    def coordonnees_aleatoire(self) :
        """
        :return: (tuple) une paire x,y désignant les coordonnées d'un élément aléatoire dans la mer
        :Exemples:
        >>> mer = Mer()
        >>> x,y = mer.coordonnees_aleatoire()
        >>> x < 5
        True
        >>> x >= 0
        True
        >>> y < 5
        True
        >>> y >= 0
        True
        """
        pass
    
    def coordonnees_aleatoire_eau(self):
        """
        :return: (tuple) une paire x,y désignant les coordonnées d'un élément aléatoire de la mer représentant de l'eau
        :Exemples:
        >>> mer = Mer()
        >>> x,y = mer.coordonnees_aleatoire()
        >>> x < 5
        True
        >>> x >= 0
        True
        >>> y < 5
        True
        >>> y >= 0
        True
        >>> mer._elements[x][y] == None
        True
        """
        pass
        
    
    def coordonnees_eaux_voisines(self,x,y) :
        """
        :param x: (int) un entier désignant une des coordonnées d'un élément
        :param y: (int) un entier désignant l'autre coordonnée de l'élément
        :return: (list) la liste des eaux voisines a l'élément dont les coordonnées sont x et y
        :CU: x >= 0 and x < self._TAILLE and y >= 0 and y < self._TAILLE
        """
        pass
    
    def coordonnees_thons_voisins(self,x,y) :
        """
        :param x: (int) un entier désignant une des coordonnées d'un élément
        :param y: (int) un entier désignant l'autre coordonnée de l'élément
        :return: (list) la liste des thons voisins a l'élément dont les coordonnées sont x et y
        :CU: x >= 0 and x < self._TAILLE and y >= 0 and y < self._TAILLE
        """
        pass
    
    def __str__(self) :
        """
        :return: (str) une chaine de caractère représentant la mer
        """
        pass
    
    
class Poisson() :
    def __init__(self) :
        pass
    
    def est_thon(self) :
        """
        :return: (bool) retourne vrai si le contenu est un thon, faux sinon
        :Exemples:
        >>> thon = Thon()
        >>> thon.est_thon()
        True
        >>> requin = Requin()
        >>> requin.est_thon()
        False
        """
        pass
    
    def est_requin(self) :
        """
        :return: (bool) retourne vrai si le contenu est un requin, faux sinon
        :Exemples:
        >>> thon = Thon()
        >>> thon.est_requin()
        False
        >>> requin = Requin()
        >>> requin.est_requin()
        True
        """
        pass
    
    def se_deplacer(self, eaux_voisines,thons_voisins) :
        """
        :param eaux_voisines: (list) la liste des coordonnées sous forme de tuple des eaux voisines au poisson
        :param thons_voisins: (list) la liste des coordonnées sous forme de tuple des thons voisins
        :return: (tuple) un couple x,y des coordonnées du poisson  après son déplacement
        """
        pass
    
    def donne_naissance(self) :
        """
        :return: (bool) retourne vrai si le poisson a donner naissance a un nouveau poisson, faux sinon
        :Exemples:
        >>> requin = Requin()
        >>> requin.donne_naissance()
        False
        >>> requin.donne_naissance()
        False
        >>> requin.donne_naissance()
        False
        >>> requin.donne_naissance()
        False
        >>> requin.donne_naissance()
        True
        >>> requin.donne_naissance()
        False
        >>> thon = Thon()
        >>> thon.donne_naissance()
        False
        >>> thon.donne_naissance()
        True
        >>> thon.donne_naissance()
        False
        """
        pass
    
    def __str__(self) :
        pass
    
class Thon(Poisson) :
    pass
    def __init__(self) :
        pass       
    
    def __str__(self) :
        """
        :return: (str) une chaine représentant le thon
        :Exemples:
        >>> thon = Thon()
        >>> str(thon)
        'T'
        """
        pass
        
class Requin(Poisson) :
    def __init__(self) :
        pass
    
    def est_mort(self) :
        """
        :return: (bool) retourne vrai si le requin est mort, faux sinon
        :Exemples:
        >>> requin = Requin()
        >>> requin.est_mort()
        False        
        """
        pass
    
    def __str__(self) :
        """
        :return: (str) une chaîne représentant le requin
        :Exemples:
        >>> requin = Requin()
        >>> str(requin)
        'R'
        """
        pass

import doctest
doctest.testmod(optionflags=doctest.NORMALIZE_WHITESPACE | doctest.ELLIPSIS, verbose=True)
