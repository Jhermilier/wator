# Simulation proie-prédateur

L'objectif de ce TP est de coder une simulation proie-prédateur avec des classes. Dans une mer, évolue des poissons qui sont soit thons (les proies) soit des requins (les prédateurs). Chacun d'entre eux se déplacent et se reproduisent. Pour acquérir l'énergie suffisante à sa survie, un requin doit manger un thon régulièrement. Un thon vit éternellement tant qu'il n'est pas mangé par un requin.

Afin d'implémenter cette simulation, compléter en respectant les différentes étapes le fichier [simulation.py](./src/simulation.py).

## Classe Poisson
Écrire une classe **Poisson** qui sera la classe mère des classes **Thon** et **Requin**. Elle doit donc implémenter les méthodes qui leur sont communes.

Le constructeur ainsi que la méthode `__str__` sont uniquement déclarer dans cette classe, en effet, les constructeurs de **Thon** et de **Requin** ainsi que leur méthode `__str__`, leur sont propre et seront donc implémentées dans les sous-classes respectives.
 

1. Implémenter les prédicats : 
- `est_thon` qui retourne vrai si le poisson est un thon, faux sinon.
- `est_requin` qui retourne vrai si le poisson est un requin, faux sinon.

2. Implémenter la méthode `se_deplacer` qui prend en paramètre 2 listes, eaux_voisines et thons_voisins, qui sont des listes de tuple de la forme(x,y) qui désignent réciproquement les coordonnées des voisins du poisson qui sont de l'eau et les coordonnées des voisins du poisson qui sont des thons.

- Si le poisson appelant cette méthode est un thon, alors la liste appelée thons_voisins passée en paramètre est vide. La méthode retourne donc un choix de tuple aléatoire parmi la liste des eaux_voisines.

- Si le poisson appelant cette méthode est un requin, on décrémente son énergie de 1 puis on distingue deux cas :
- Si la liste des thons voisins n'est pas vide, alors cela indique que le requin va manger un thon, et incrémente alors de 2 l'énergie de celui-ci.
- Sinon, la liste est vide et le comportement est le même que pour le thon.

3. Sachant qu'un poisson donne naissance à un nouveau poisson lorsque son temps de gestation atteint 0, et qu'un poisson peut faire naître un seul poisson a la fois, mais qu'il peut donner naissance à plusieurs poissons dans sa vie :
Implémenter la méthode `donne_naissance` qui dans un premier temps décrémente l'attribut _temps_gestation. Puis retourne vrai si le poisson donne naissance à un nouveau poisson, faux sinon.



## Classe Thon
La classe **Thon** hérite de la classe **Poisson**.


1. Déclarer un attribut de classe _DUREE_GESTATION que nous fixerons à la valeur 2.

2. Écrire le constructeur de la classe qui initialise l'attribut _temps_gestation à la valeur _DUREE_GESTATION.

3. Coder la méthode `__str__` qui retourne la chaîne de caractère représentant le thon.



## Classe Requin
La classe Requin est une sous-classe de la classe Poisson.


1. Déclarer les attributs de classe _DUREE_GESTATION et _ENERGIE qui seront respectivement fixées à 5 et 3.

2. Implémenter le constructeur de cette classe qui initialise les attributs _temps_gestation du requin a _DUREE_GESTATION et _energie à _ENERGIE.

3. Sachant qu'un requin meurt lorsqu'il n'a plus d'énergie, coder le prédicat est_mort qui retourne vrai si le requin est mort, faux sinon.

4. Implémenter la méthode `__str__` qui retourne la chaine de caractere representant le requin.


## Classe Mer
La **Mer** est représentée par une grille à deux dimensions de taille _TAILLE fixée. Cette grille est torique c'est a dire que chaque case à quatre voisines, une dans chacune des quatre directions. 

![](./img/schema_mer.png)

Chacune des cases de cette grille est remplie par un poisson ou de l'eau. Lorsqu'une case contient de l'eau, on admet que cette case est vide et sera implémenter par la valeur None.


1. Déclarer l'attribut de classe _TAILLE dont la valeur est fixée à 5 et qui représente la taille de la mer.

2. Implémenter le constructeur de la classe, qui instancie un attribut _elements qui est une liste à deux dimensions de dimension _TAILLE * _TAILLE, avec tout ses éléments initialiser à None.

3. Implémenter la méthode `coordonnees_aleatoire` qui retourne un tuple composé de deux entiers aléatoires désignant un élément de la mer.

4. Implémenter la méthode `coordonnees_aleatoire_eau` qui retourne un tuple de deux entiers aléatoires désignant un élément aléatoire de la mer contenant de l'eau.
On pourra faire appel a la méthode `coordonnees_aleatoire` développée précédamment.

5. Implémenter la méthode `coordonnee_eaux_voisines` qui prend en paramètre deux entiers x et y qui sont les coordonnées d'un élément de la mer et qui retourne la liste des coordonnées des eaux voisines a l'élément sous forme de tuple. 

6. Implémenter de la même manière la méthode `coordonnees_thons_voisins` qui prend en paramètre deux entiers x et y qui sont les coordonnées d'un élément de la mer et qui retourne la liste des coordonnées des thons voisins a l'élément sous forme de tuple.

7. Implémenter la `__str__` qui retourne une chaîne de caractère représentant tous les éléments présents dans la mer.
- Les lignes de la grille sont séparées par un retour a la ligne.
- Chacun des éléments d'une ligne sont séparer par un  espace.
- On fixe le caractère '_' pour représenter l'eau.

 
## Classe Simulation

1. Déclarer les attributs de classe _NB_THONS et _NB_REQUINS respectivement fixés aux valeurs 8 et 1.

2. Implémenter le constructeur de cette classe qui prend en paramètre un entier n. Il initialise un attribut _nb_simulations à la valeur n, un attribut _mer de type **Mer**. 

3. Implémenter les méthodes `init_thons` et `init_requins` qui instancient dans la mer respectivement _NB_THONS Thons et _NB_REQUINS requins dans un emplacement de la mer contenant de l'eau.
Modifier le constructeur de la classe **Simulation**, de manière à ce qu'il fasse appel à ces deux dernières méthodes.

4. Implémenter la méthode `simuler_un_tour` de sorte que :
On prend un élément aléatoire dans la mer,
- Si l'élement choisi est de l'eau, il ne se passe rien

- Si l'élément choisi est un **Thon**,
	il se déplace si cela est possible, c'est à dire si un de l'on fait appel sa méthode `se_deplacer` en donnant comme paramètres la liste de ses eaux voisines et la liste vide qui représente les thons voisins, le tuple d'entier renvoyer par cette méthode sont les nouvelles coordonnées du thon dans la mer
	Si il donne naissance, un nouveau thon est instancier a la position precedante du thon parent
	Sinon le thon est remplacé par de l'eau

- Si l'élément choisi est un **Requin**,
	il se déplace c'est à dire que l'on fait appel sa méthode `se_deplacer` en donnant comme paramètre la liste de ses eaux voisines et la liste de ces thons voisins, le tuple d'entier renvoyer par cette méthode sont les nouvelles coordonnées du requin dans la mer
	Si il donne naissance, un nouveau requin est instancier a la position précédante du requin parent.
	Sinon le requin est remplacé par de l'eau
	Si le requin est mort, alors de l'eau vient remplacer le requin dans la mer.

5. Implémenter la methode `lancer_simulation` qui dans un premier temps, affiche la mer puis effectue _nb_simulation simulations une à une. Apres chacun tour simulé, cette methode affiche le numero de tour ainsi que la mer.


Instancier simulation, une instance de la classe **Simulation**, vous passerait en paramètre le nombre de tours que vous souhaiter.

Lancer la simulation et observer le comportement de vos poissons.

